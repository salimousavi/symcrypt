# Database config
MONGO_HOST = 'localhost'
MONGO_PORT = 27017


# forgot password settings
EXPIRED_TIME = 5 *60
MIN_SIZE_TOKEN = 24
MAX_SIZE_TOKEN = 48

# API Config
SECRET_KEY = 'Something Secret'

# Modules
INSTALLED_MODULES = [
    'user',
    'crypto',
    # 'price'
]

