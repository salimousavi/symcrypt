import importlib
import inspect
import datetime
import string
from random import *

from flask import Flask, session, g, request, jsonify, abort, url_for
from flask_login import LoginManager, login_user
from flask_restful import Api

from mongoengine import connect
from symcrypt.user.models import UserModel, VerificationCodeModel
from symcrypt.conf import MONGO_HOST, MONGO_PORT, SECRET_KEY, INSTALLED_MODULES, EXPIRED_TIME, MIN_SIZE_TOKEN, MAX_SIZE_TOKEN


connect('symcrypt', host=MONGO_HOST, port=MONGO_PORT)


app = Flask(__name__)
app.secret_key = SECRET_KEY

login_manager = LoginManager()
login_manager.init_app(app)

api = Api(app)


@login_manager.user_loader
def load_user(user):
    return UserModel.objects(username=user).first()


@app.route('/user/login', methods=['POST'])
def login():
    u = UserModel.objects(username=request.form['username']).first()

    if u:
        if u.password == request.form['password']:
            login_user(u) 
            return jsonify(
                {
                    'err': '',
                }
            ), 200
        else:
            return jsonify(
                {
                    'err': 'username or password is not correct',
                }
            ), 400
    else:
        return jsonify(
            {
                'err': 'the user does not exist'
            }
        ), 404



@app.route('/user/forget_password', methods=['GET'])
def forget_password():
    u = UserModel.objects(username=request.args.get('username')).first()

    if u:
        code = request.args.get('code')

        if code:

            vc = VerificationCodeModel.objects(code=code, email=u.username).first()
            if vc:

                code_date = vc.date
                now = datetime.datetime.now()

                if (now - code_date).total_seconds() < EXPIRED_TIME:
                    login_user(u)
                    # TODO: Redirect user to change password page or activity or send user token to client
                    return jsonify(
                        {
                            'err': '',
                        }
                    ), 200

                else:
                    return jsonify(
                        {
                            'err': 'code has expired'
                        }
                    ), 404


            else:
                return jsonify(
                    {
                        'err': 'the email or code is incorrect'
                    }
                ), 404

        else:

            c = "".join(choice(string.digits) for x in range(randint(MIN_SIZE_TOKEN, MAX_SIZE_TOKEN)))

            vc = VerificationCodeModel()
            vc.email = u.username
            vc.date = datetime.datetime.now()
            vc.code = c
            vc.save()

            print(c)
            # TODO : Send code by email


            return jsonify(
                {
                    'err': '',
                }
            ), 200

    else:
        return jsonify(
            {
                'err': 'the user does not exist'
            }
        ), 404


for pkg in INSTALLED_MODULES:
    service_script = importlib.import_module(
        "symcrypt.%s.resources" % pkg)
    admin_script = importlib.import_module('symcrypt.%s.models' % pkg)
    for name, obj in inspect.getmembers(service_script):
        if inspect.isclass(obj) and 'symcrypt.%s' % pkg in str(obj) and 'Resource' in str(obj):
            api.add_resource(
                obj,
                '/%s/%s' % (pkg, obj.url),
                endpoint='%s.%s' % (pkg, obj.__name__)
            )
    for name, obj in inspect.getmembers(admin_script):
        if inspect.isclass(obj) and 'api.%s' % pkg in str(obj) and 'Model' in str(obj):
            admin.add_view(ModelView(obj))
